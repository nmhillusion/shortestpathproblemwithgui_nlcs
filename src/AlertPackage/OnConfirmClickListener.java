package AlertPackage;

/**
 *
 * @author nmhillusion
 */
public interface OnConfirmClickListener {
    void onPositive();
    void onNegative();
}

package AlertPackage;

/**
 *
 * @author nmhillusion
 */
public interface OnPromptClickListener {
    void onPositive(String val);
    void onNegative();
    void onNullValue();
}

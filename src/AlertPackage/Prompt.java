package AlertPackage;

import ShortestPathProplemWithGUI.ShortestPathProplemWithGUI;
import java.util.Optional;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author nmhillusion
 */
public class Prompt {
    private String title, headerText, content, defaultInput = "";
    private OnPromptClickListener listener;
    private final static Prompt INSTANCE = new Prompt();
    private boolean isAutoButton = false;
    
    private Prompt(){}
    public static Prompt getIntance(){
        return INSTANCE;
    }

    public Prompt setTitle(String title) {
        this.title = title;
        return this;
    }

    public Prompt setHeaderText(String headerText) {
        this.headerText = headerText;
        return this;
    }

    public Prompt setContent(String content) {
        this.content = content;
        return this;
    }
    
    public Prompt setDefaultInput(String defaultInput){
        this.defaultInput = defaultInput;
        return this;
    }

    public Prompt setListener(OnPromptClickListener listener) {
        this.listener = listener;
        return this;
    }
    
    public Prompt setAutoButton(boolean val){
        this.isAutoButton = val;
        return this;
    }
    
    public void show(){        
        Dialog<ButtonType> dialog = new Dialog();
        ((Stage)dialog.getDialogPane().getScene().getWindow()).getIcons().add(ShortestPathProplemWithGUI.getIcon());
        dialog.getDialogPane().setPrefWidth(600);
        
        VBox dialogPane = new VBox();
        ButtonType btnOk = ButtonType.OK;
        
        TextField inputField = new TextField();
        inputField.setPromptText("input value here....");
        inputField.setText(defaultInput);
        Platform.runLater(()->{inputField.requestFocus();});

        ButtonType btnAuto = new ButtonType("Auto", ButtonBar.ButtonData.FINISH);
        if(isAutoButton){
            dialog.getDialogPane().getButtonTypes().add(btnAuto);
            Platform.runLater(()->{dialog.getDialogPane().lookupButton(btnAuto).requestFocus();});
        }
        
        inputField.textProperty().addListener((observable, oldValue, newValue) -> {
            dialog.getDialogPane().lookupButton(btnOk).setDisable(newValue.trim().isEmpty());
            if(dialog.getDialogPane().getButtonTypes().contains(btnAuto) && !newValue.trim().isEmpty()){
                ((Button)dialog.getDialogPane().lookupButton(btnOk)).setDefaultButton(true);
                ((Button)dialog.getDialogPane().lookupButton(btnAuto)).setDefaultButton(false);
            }
        });
        
        dialogPane.getChildren().add(inputField);
        dialog.getDialogPane().getButtonTypes().addAll(btnOk,ButtonType.CANCEL);
        dialog.getDialogPane().lookupButton(btnOk).setDisable(true);
        
        dialog.getDialogPane().setContent(dialogPane);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(content);
        
        Optional<ButtonType> res =  dialog.showAndWait();
        if(res.isPresent()){
            if(res.get() == btnAuto){
                listener.onNullValue();
            }else if(res.get() == btnOk){
                if(inputField.getText() != null && inputField.getText().trim().length() > 0){
                    listener.onPositive(inputField.getText().trim());
                }else{
                    listener.onNegative();
                }
            }else{
                listener.onNegative();
            }
        }
        
        defaultInput = headerText = title = content = null;
        listener = null;
        isAutoButton = false;
    }
}
package AlertPackage;

import ShortestPathProplemWithGUI.ShortestPathProplemWithGUI;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

/**
 *
 * @author nmhillusion
 */
public class Confirm {
    private String title, headerText, content;
    private OnConfirmClickListener listener;
    private final static Confirm INSTANCE = new Confirm();
    
    private Confirm(){}
    public static Confirm getIntance(){
        return INSTANCE;
    }

    public Confirm setTitle(String title) {
        this.title = title;
        return this;
    }

    public Confirm setHeaderText(String headerText) {
        this.headerText = headerText;
        return this;
    }

    public Confirm setContent(String content) {
        this.content = content;
        return this;
    }

    public Confirm setListener(OnConfirmClickListener listener) {
        this.listener = listener;
        return this;
    }
    
    public void show(){
        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION);
        ((Stage)confirm.getDialogPane().getScene().getWindow()).getIcons().add(ShortestPathProplemWithGUI.getIcon());
        confirm.getDialogPane().setPrefWidth(600);
        
        confirm.setHeaderText(headerText);
        confirm.setTitle(title);
        confirm.setContentText(content);
        confirm.setResult(ButtonType.CANCEL);
        Optional<ButtonType> res = confirm.showAndWait();
        if(listener != null && res.isPresent()){
            if(res.get().getButtonData() == ButtonBar.ButtonData.OK_DONE){
                listener.onPositive();
            }else{
                listener.onNegative();
            }
        }
        
        // init value
        headerText = title = content = "";
        listener = null;
    }
}
package UtilsPackage;

import GraphComponent.Graph;
import GraphComponent.Vertex;
import java.util.List;
import javafx.animation.FillTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Cursor;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 *
 * @author nmhillusion
 */
public class GraphUtils {

    private final static GraphUtils INSTANCE = new GraphUtils();
    private int idxTraversalVertex = 0,
            idxVertexMove = 0;

    private FillTransition traversalAnimation;
    private TranslateTransition pathMoveAnimation;
    private final AnimationListener animationListener = new AnimationListener() {
        @Override
        public synchronized void onStop() {
            stopped.setValue(Boolean.TRUE);
            if (traversalAnimation != null) {
                traversalAnimation.stop();
            }
            if (pathMoveAnimation != null) {
                pathMoveAnimation.stop();
            }
        }
    };
    private SimpleBooleanProperty stopped = new SimpleBooleanProperty(false);

    private GraphUtils() {
    }

    public static GraphUtils getInstance() {
        return INSTANCE;
    }

    public void moveText(Text text) {
        text.setOnDragDetected((event) -> {
            text.startFullDrag();
            text.startDragAndDrop(TransferMode.ANY);
            text.setCursor(Cursor.CLOSED_HAND);
        });

        text.setOnMouseDragged((event) -> {
            text.setX(event.getX());
            text.setY(event.getY());
        });

        text.setOnMouseDragReleased((event) -> {
            text.setCursor(Cursor.DEFAULT);
        });
    }

    /**
     * Calculates the angle (in radians) between two vectors pointing outward
     * from one center
     *
     * @param p0 first point
     * @param p1 second point
     * @param c center point
     * @return angle between c->p0 and c->p1
     */
    public double find_angle(Vertex p0, Vertex p1, Vertex c) {
        double p0c = Math.sqrt(Math.pow(c.getX() - p0.getX(), 2)
                + Math.pow(c.getY() - p0.getY(), 2)); // p0->c (b)   
        double p1c = Math.sqrt(Math.pow(c.getX() - p1.getX(), 2)
                + Math.pow(c.getY() - p1.getY(), 2)); // p1->c (a)
        double p0p1 = Math.sqrt(Math.pow(p1.getX() - p0.getX(), 2)
                + Math.pow(p1.getY() - p0.getY(), 2)); // p0->p1 (c)
        return Math.acos((p1c * p1c + p0c * p0c - p0p1 * p0p1) / (2 * p1c * p0c)) * (180 / Math.PI) * (p1.getY() - c.getY() > 0 ? 1 : -1);
    }

    public AnimationListener getAnimationListener() {
        return animationListener;
    }

    public void traversalAnimation(final List<Vertex> verticesList) {
        if (verticesList.isEmpty()) {
            return;
        } else {
            stopped.setValue(Boolean.FALSE);
        }

        idxTraversalVertex = 0;
        traversalAnimation = new FillTransition(Duration.seconds(2), verticesList.get(idxTraversalVertex), Color.RED, Color.AZURE);
        verticesList.get(idxTraversalVertex).getNameView().getStyleClass().add("show");
        traversalAnimation.setOnFinished((event) -> {
            if (idxTraversalVertex < verticesList.size()) {
                verticesList.get(idxTraversalVertex).getNameView().getStyleClass().remove("show");
            }
            if (++idxTraversalVertex < verticesList.size()) {
                traversalAnimation.setShape(verticesList.get(idxTraversalVertex));
                traversalAnimation.play();
                verticesList.get(idxTraversalVertex).getNameView().getStyleClass().add("show");
            }
        });
        traversalAnimation.play();

        stopped.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                verticesList.forEach((v) -> {
                    v.getNameView().getStyleClass().remove("show");
                    v.setFill(Paint.valueOf("#359bed"));
                });
            }
        });
    }

    public void pathMoveAnimation(List<Vertex> verticesList, Graph graph) {
        if (verticesList.isEmpty()) {
            return;
        } else {
            stopped.setValue(Boolean.FALSE);
        }

        idxVertexMove = 0;
        if (idxVertexMove >= 0 && idxVertexMove + 1 < verticesList.size()) {
            Vertex fromVertex = verticesList.get(idxVertexMove),
                    toVertex = verticesList.get(idxVertexMove + 1);

            final ImageView viewpoint = new ImageView(getClass().getResource("/Resource/Archlinux-Icon.png").toExternalForm());
            viewpoint.setFitWidth(20);
            viewpoint.setFitHeight(20);
            viewpoint.setEffect(new DropShadow(5, Color.web("#359bed")));
            viewpoint.setRotate(find_angle(new Vertex(fromVertex.getX() + 20, fromVertex.getCenterY(), null), toVertex, fromVertex) + 90);

            graph.getChildren().add(viewpoint);
            pathMoveAnimation = new TranslateTransition(Duration.seconds(2), viewpoint);

            pathMoveAnimation.setFromX(fromVertex.getX() - 10);
            pathMoveAnimation.setFromY(fromVertex.getY() - 10);
            pathMoveAnimation.setToX(toVertex.getX() - 10);
            pathMoveAnimation.setToY(toVertex.getY() - 10);

            fromVertex.getNameView().getStyleClass().add("show");
            pathMoveAnimation.setOnFinished((event) -> {
                if (++idxVertexMove < verticesList.size() - 1) {
                    Vertex _fromVertex = verticesList.get(idxVertexMove),
                            _toVertex = verticesList.get(idxVertexMove + 1);
                    _fromVertex.getNameView().getStyleClass().add("show");

                    viewpoint.setRotate(find_angle(new Vertex(_fromVertex.getX() + 20, _fromVertex.getCenterY(), null), _toVertex, _fromVertex) + 90);

                    pathMoveAnimation.setFromX(_fromVertex.getX() - 10);
                    pathMoveAnimation.setFromY(_fromVertex.getY() - 10);
                    pathMoveAnimation.setToX(_toVertex.getX() - 10);
                    pathMoveAnimation.setToY(_toVertex.getY() - 10);
                    pathMoveAnimation.play();
                } else {
                    graph.getChildren().remove(viewpoint);
                    if (idxVertexMove < verticesList.size()) {
                        verticesList.get(idxVertexMove).getNameView().getStyleClass().add("show");
                    }
                }
            });

            pathMoveAnimation.play();

            stopped.addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    graph.getChildren().remove(viewpoint);
                    verticesList.forEach((vertex) -> {
                        vertex.getNameView().getStyleClass().remove("show");
                    });
                }
            });
        }
    }
}

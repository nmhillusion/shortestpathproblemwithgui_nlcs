package UtilsPackage;

import AlertPackage.Alert;
import GraphComponent.Edge;
import GraphComponent.Graph;
import GraphComponent.Vertex;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.geometry.Pos;
import org.controlsfx.control.Notifications;

/**
 *
 * @author nmhillusion
 */
public class FileUtils {

    private static final FileUtils INSTANCE = new FileUtils();

    private FileUtils() {
    }

    public static FileUtils getIntance() {
        return INSTANCE;
    }

    public Graph readFromFile(String path) {
        Graph graph = new Graph();

        List<Vertex> vertexList = new ArrayList<>();
        List<Edge> edgeList = new ArrayList<>();

        Pattern vertexPattern = Pattern.compile("vertex<<name: (.+?), x: (.+?), y: (.+?)>>"),
                edgePattern = Pattern.compile("edge<<startV: (.+?), endV: (.+?), weight: (.+?)>>"),
                settingPattern = Pattern.compile("setting<<backgroundUrl: (.+?), skin: (.+?)>>");
        Matcher matcher;
        Scanner scanner;
        try {
            scanner = new Scanner(new File(path));
            scanner.useDelimiter(";");
            String next;
            while (scanner.hasNext(vertexPattern)) {
                next = scanner.next();
                matcher = vertexPattern.matcher(next);
                while (matcher.find()) {
                    vertexList.add(new Vertex(Double.valueOf(matcher.group(2)), Double.valueOf(matcher.group(3)), matcher.group(1), graph));
                }
            }

            while (scanner.hasNext(edgePattern)) {
                next = scanner.next();
                matcher = edgePattern.matcher(next);
                while (matcher.find()) {
                    Vertex startV = findVertexByName(vertexList, matcher.group(1)),
                            endV = findVertexByName(vertexList, matcher.group(2));
                    if (startV == null || endV == null) {
                        Alert.getIntance()
                                .setTitle("Error in import file")
                                .setHeaderText("Can not find vertex to create edge")
                                .show();
                    } else {
                        Edge edge = new Edge(startV, endV, graph);
                        edge.setWeight(Double.valueOf(matcher.group(3)));
                        edgeList.add(edge);
                    }
                }
            }

            if (scanner.hasNext(settingPattern)) {
                next = scanner.next();
                matcher = settingPattern.matcher(next);
                while (matcher.find()) {
                    Setting.setBackgroundUrl(matcher.group(1));
                    Setting.setTypeOfSkin(0 == matcher.group(2).compareTo("DARK") ? Setting.SKIN.DARK : Setting.SKIN.LIGHT);
                }
            } else {
                Setting.setBackgroundUrl("");
                Setting.setTypeOfSkin(Setting.SKIN.LIGHT);
            }
            scanner.close();
        } catch (Throwable ex) {
            Alert.getIntance()
                    .setTitle("Error in import")
                    .setHeaderText("Raise an exception in import file: " + ex.getMessage());
        }

        graph.setAllVertex(vertexList);
        graph.setAllEdge(edgeList);
        return graph;
    }

    private Vertex findVertexByName(List<Vertex> vertexList, String name) {
        for (Vertex v : vertexList) {
            if (name.compareTo(v.getName()) == 0) {
                return v;
            }
        }
        return null;
    }

    public boolean saveGraph(String path, Graph graph) {
        File file = new File(path);

        try {
            if (file.exists()) {
                file.createNewFile();
            }
            PrintWriter printWriter = new PrintWriter(file);
            graph.getListVertex().forEach((vertex) -> {
                printWriter.print("vertex<<name: " + vertex.getName() + ", x: " + vertex.getX() + ", y: " + vertex.getY() + ">>;");
            });
            graph.getListEdge().forEach((edge) -> {
                printWriter.print("edge<<startV: " + edge.getStartV().getName() + ", endV: " + edge.getEndV().getName() + ", weight: " + edge.getWeight() + ">>;");
            });

            printWriter.print("setting<<backgroundUrl: " + Setting.getBackgroundUrl() + ", skin: " + Setting.getTypeOfSkin() + ">>;");

            printWriter.close();

            Notifications.create()
                    .title("Sucess Saving!")
                    .text("Saved your graph in: " + file.getAbsolutePath())
                    .position(Pos.BOTTOM_RIGHT)
                    .showInformation();
        } catch (Throwable ex) {
            Alert.getIntance()
                    .setTitle("Error in export file")
                    .setHeaderText("Raise an exception: " + ex.getMessage())
                    .show();
        }

        return file.exists();
    }
}

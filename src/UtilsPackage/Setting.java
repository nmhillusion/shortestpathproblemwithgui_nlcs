package UtilsPackage;

import ShortestPathProplemWithGUI.ShortestPathProplemWithGUI;
import java.io.File;
import java.util.Optional;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author nmhillusion
 */
public class Setting {
    private final static Setting INSTANCE = new Setting();
    private static String backgroundUrl = null;
    
    private Setting(){}
    public static Setting getInstance(){
        return INSTANCE;
    }

    public static String getBackgroundUrl() {
        return backgroundUrl;
    }

    public static SKIN getTypeOfSkin() {
        return typeOfSkin;
    }

    public static void setBackgroundUrl(String aBackgroundUrl) {
        backgroundUrl = aBackgroundUrl;
    }

    public static void setTypeOfSkin(SKIN aTypeOfSkin) {
        typeOfSkin = aTypeOfSkin;
    }

    public static enum SKIN {
        LIGHT,
        DARK
    };
    private static SKIN typeOfSkin = SKIN.LIGHT;
    
    public interface OnCloseSetting{
        void onClose();
    }

    public void openSetting(OnCloseSetting onCloseSetting) {
        Dialog<ButtonType> dialog = new Dialog<>();

        GridPane mainContent = new GridPane();
        Button  btnChooseFile = new Button("Choose File"),
                btnClean = new Button("delete");
        TextField backgroundInput = new TextField(backgroundUrl);
        
        btnClean.setId("btnClean");
        btnChooseFile.setId("btnChooseFile");
        
        backgroundInput.setPromptText("this graph has no map");
        backgroundInput.setDisable(true);
        mainContent.addRow(0, new Text("Set Map"), backgroundInput, btnClean,btnChooseFile);
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image", "*.png", "*.jpg", "*.jpeg", "*.gif"));
        fileChooser.setInitialDirectory(new File("."));
        btnChooseFile.setOnAction((event) -> {
            File file = fileChooser.showOpenDialog(dialog.getDialogPane().getScene().getWindow());
            if (file != null) {
                backgroundInput.setText(file.getAbsolutePath());
            }
        });
        btnClean.setOnAction((event) -> {
            backgroundInput.setText("");
        });

        ComboBox<String> skinChoice = new ComboBox<>();
        skinChoice.getItems().addAll("LIGHT", "DARK");
        skinChoice.getSelectionModel().select(typeOfSkin == SKIN.DARK ? 1 : 0);
        mainContent.add(new Text("Skin of Graph"), 0, 1, 1, 1);
        mainContent.add(skinChoice, 1, 1, 2, 1);

        dialog.getDialogPane().setContent(mainContent);
        dialog.setTitle("Setting Graph");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.FINISH, ButtonType.CANCEL);
        
        dialog.getDialogPane().getScene().getStylesheets().add(getClass().getResource("/Resource/styleSetting.css").toExternalForm());
        ((Stage) dialog.getDialogPane().getScene().getWindow()).getIcons().add(ShortestPathProplemWithGUI.getIcon());

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.FINISH) {
            backgroundUrl = backgroundInput.getText();
            typeOfSkin = skinChoice.getValue().compareTo("LIGHT") == 0 ? SKIN.LIGHT : SKIN.DARK;
            
            if(onCloseSetting != null){
                onCloseSetting.onClose();
            }
        }
    }
}

package GraphComponent.ListenerPackage;

import GraphComponent.Edge;

/**
 *
 * @author nmhillusion
 */
public interface OnCreateEdgeListener {
    void onCreateEdge(Edge edge);
}
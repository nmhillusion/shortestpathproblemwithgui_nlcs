package GraphComponent.ListenerPackage;

import GraphComponent.Edge;

/**
 *
 * @author nmhillusion
 */
public interface OnEditEdge {
    void onDeleteEdge(Edge edge);
}

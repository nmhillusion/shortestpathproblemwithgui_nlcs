package GraphComponent.ListenerPackage;

import GraphComponent.Vertex;
import javafx.scene.input.MouseDragEvent;

/**
 *
 * @author nmhillusion
 */
public interface OnDragAndDropVertexListener {
    void onStartDrag(Vertex startVertex);
    void onDragOver();
    void onDrop(MouseDragEvent event);
}

package GraphComponent.ListenerPackage;

import GraphComponent.Vertex;

/**
 *
 * @author nmhillusion
 */
public interface OnTraversalVertex {
    void traversal(Vertex vertex);
}

package GraphComponent.ListenerPackage;

import GraphComponent.Vertex;

/**
 *
 * @author nmhillusion
 */
public interface OnEditVertex {
    void onMoveVertex(Vertex vertex);
    void onDeleteVertex(Vertex vertex);
    void onSetSourceVertex(Vertex vertex);
    void onSetTargetVertex(Vertex vertex);
}

package GraphComponent;

import AlertPackage.Alert;
import AlertPackage.Confirm;
import AlertPackage.OnConfirmClickListener;
import AlertPackage.OnPromptClickListener;
import AlertPackage.Prompt;
import GraphComponent.ListenerPackage.OnCreateEdgeListener;
import GraphComponent.ListenerPackage.OnDragAndDropVertexListener;
import GraphComponent.ListenerPackage.OnEditVertex;
import UtilsPackage.GraphUtils;
import ShortestPathProplemWithGUI.ShortestPathProplemWithGUI;
import ShortestPathProplemWithGUI.MODE_MENU;
import UtilsPackage.Setting;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.TransferMode;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 *
 * @author nmhillusion
 */
public class Vertex extends Circle implements Comparable<Vertex> {

    private static Vertex dragVertex;
    private Text nameView;
    private Graph graph;
    private double x, y;
    private String name;
    private boolean isMarked = false;
    private final ContextMenu contextMenu = new ContextMenu();

    /**
     * parent vertex of this vertex on the path
     */
    private Vertex parentVertex;
    /**
     * shortest distance from source vertex
     */
    private double sDistance;

    public Vertex(double x, double y, Graph graph) {
        super(x, y, 5);
        this.x = x;
        this.y = y;
        getStyleClass().add("vertex");

        if (graph != null) {
            this.graph = graph;
            this.nameView = new Text(x, y - 20, "");
            this.nameView.getStyleClass().add("vertex-name");
            graph.getChildren().add(this.nameView);
            GraphUtils.getInstance()
                    .moveText(this.nameView);
            refreshSkin();
        }

        contextMenu.setAutoHide(true);

        /**
         * set configuration for drag and drop
         */
        setOnDragDetected((event) -> {
            startFullDrag();
            startDragAndDrop(TransferMode.ANY);

            dragVertex = this;
            event.setDragDetect(true);
            if (graph instanceof OnDragAndDropVertexListener) {
                graph.onStartDrag(this);
            }

            event.consume();
        });

        setOnMouseDragEntered((event) -> {
            setRadius(7);
            getStyleClass().add("over");
            event.consume();

            if (graph instanceof OnDragAndDropVertexListener) {
                graph.onDragOver();
            }
        });

        setOnMouseDragExited((event) -> {
            getStyleClass().remove("over");
            setRadius(5);
            event.consume();
        });

        setOnMouseDragReleased((event) -> {
            if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.BUILDING &&
                    dragVertex instanceof Vertex) {
                if (graph instanceof OnCreateEdgeListener) {
                    graph.onCreateEdge(new Edge(this, dragVertex, name + " - " + dragVertex.getName(), graph));
                    
                    dragVertex = null;
                }
                if (graph instanceof OnDragAndDropVertexListener) {
                    graph.onDrop(event);
                }
            }
            event.consume();
        });

        setOnMouseDragged((event) -> {
//          ...dragging...
            if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EDITTING) {
                this.x = event.getX();
                this.y = event.getY();
                setCenterX(this.x);
                setCenterY(this.y);
                this.nameView.setX(event.getX());
                this.nameView.setY(event.getY() - 20);

                if (graph instanceof OnEditVertex) {
                    graph.onMoveVertex(this);
                }
            }
        });

        setOnMousePressed((event) -> {
            if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EDITTING
                    || ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EXECUTION) {
                if (!event.isDragDetect() && graph instanceof Graph) {
                    graph.getListVertex().forEach((v) -> {
                        if (!v.isSame(this)) {
                            v.setSelected(false);
                        }
                    });
                    if (event.isSecondaryButtonDown()) {
                        openContextMenu(event.getScreenX(), event.getScreenY());
                        setSelected(true);
                    }else if (event.isPrimaryButtonDown()) {
                        setSelected(!isSelected());
                    }
                }
            }
        });
    }

    public Vertex(double x, double y, String name, Graph graph) {
        this(x, y, graph);
        setName(name);
    }
    
    /**
     * load and set again skin for name of this vertex
     */
    public void refreshSkin(){
        if(nameView != null && Setting.getTypeOfSkin() == Setting.SKIN.DARK){
            nameView.getStyleClass().add("dark-skin");
        }else{
            nameView.getStyleClass().remove("dark-skin");
        }
    }

    private void reqName() {
        Prompt.getIntance()
                .setTitle("Rename Vertex")
                .setHeaderText("Please input new vertex's name")
                .setDefaultInput(name)
                .setListener(new OnPromptClickListener() {
                    @Override
                    public void onPositive(String val) {
                        if (!val.matches("([aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ0-9\\_\\-\\s])*")) {
                            Alert.getIntance()
                                    .setTitle("Error in set name")
                                    .setContent("Name is only contains characters, numbers, space, undercore or dash.")
                                    .show();
                            reqName();
                            return;
                        }
                        setName(val);
                        SAME_TYPE_VERTEX typeSame = graph.containsVertex(Vertex.this);
                        if (typeSame == SAME_TYPE_VERTEX.SAME_NAME) {
                            Alert.getIntance()
                                    .setTitle("Error")
                                    .setHeaderText("This name vertex has existed! Please change it...")
                                    .setContent("Please input another name!")
                                    .show();
                            reqName();
                        }else{
                            ShortestPathProplemWithGUI.getInstance()
                                .sendMessageOutput("edited name of " + Vertex.this);
                        }
                    }

                    @Override
                    public void onNegative() {
                    }

                    @Override
                    public void onNullValue() {
                        Alert.getIntance()
                                .setTitle("Error")
                                .setHeaderText("Name can not null")
                                .setContent("Please fill name of vertex!")
                                .show();
                        reqName();
                    }
                }).show();
    }

    private void openContextMenu(double x, double y) {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EDITTING) {
            // open menu edit
            MenuItem btnEditName = new MenuItem("Edit Name"),
                    btnDelete = new MenuItem("Delete");

            btnEditName.setOnAction((event) -> {
                // edit name of this vertex
                reqName();
            });

            btnDelete.setOnAction((event) -> {
                // delete this vertex and edges which contains it
                Confirm.getIntance()
                        .setTitle("Confirm DELETE Vertex")
                        .setHeaderText("You really want to delete this vertex? " + this)
                        .setListener(new OnConfirmClickListener() {
                            @Override
                            public void onPositive() {
                                try {
                                    graph.onDeleteVertex(Vertex.this);
                                } catch (Throwable ex) {
                                    Alert.getIntance()
                                            .setTitle("Error in delete Vertex")
                                            .setHeaderText("Raise an exception: " + ex.getMessage());
                                }
                            }

                            @Override
                            public void onNegative() {

                            }
                        }).show();
            });

            contextMenu.getItems().setAll(btnEditName, btnDelete);
        } else if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EXECUTION) {
            // open menu set source and target vertex
            MenuItem btnSetSource = new MenuItem("Set as Source Vertex"),
                    btnSetTarget = new MenuItem("Set as Target Vertex");

            btnSetSource.setOnAction((event) -> {
                // set source vertex
                if (graph instanceof OnEditVertex) {
                    graph.onSetSourceVertex(Vertex.this);
                    setSourceVertex(true);
                }
            });

            btnSetTarget.setOnAction((event) -> {
                // set target vertex
                if (graph instanceof OnEditVertex) {
                    graph.onSetTargetVertex(Vertex.this);
                    setTargetVertex(true);
                }
            });

            contextMenu.getItems().setAll(btnSetSource, btnSetTarget);
        }
        contextMenu.show(this, x, y);
    }

    @Override
    public String toString() {
        return "vertex " + name + " (" + x + "," + y + ")";
    }

    /**
     * get name of this vertex
     *
     * @return name vertex
     */
    public String getName() {
        return name;
    }

    /**
     * get x axis of this vertex
     *
     * @return x axis
     */
    public double getX() {
        return x;
    }

    /**
     * get y axis of this vertex
     *
     * @return y axis
     */
    public double getY() {
        return y;
    }

    /**
     * get vertex which start drag
     *
     * @return vertex which start drag
     */
    public static Vertex getDragVertex() {
        return dragVertex;
    }

    /**
     * set name for this vertex
     *
     * @param name name vertex
     */
    public void setName(String name) {
        this.name = name.trim();
        this.nameView.setText(this.name);
    }

    /**
     * check same between two vertex
     *
     * @param another another vertex to check
     * @return is same?
     */
    public SAME_TYPE_VERTEX getSameType(Vertex another) {
        if (x == another.x && y == another.y) {
            return SAME_TYPE_VERTEX.SAME_COORDINATE;
        } else if (name.compareTo(another.name) == 0) {
            return SAME_TYPE_VERTEX.SAME_NAME;
        } else {
            return SAME_TYPE_VERTEX.NOT_SAME;
        }
    }

    /**
     * Check the equality between two vertex
     *
     * @param another this to check with this object
     * @return is same
     */
    public boolean isSame(Vertex another) {
        if (!(another instanceof Vertex)) {
            return false;
        }
        return (x == another.x && y == another.y) || (name != null && another.name != null && name.compareTo(another.name) == 0);
    }

    /**
     * check mark status of this vertex
     *
     * @return is marked
     */
    public boolean isMarked() {
        return isMarked;
    }

    /**
     * set mark status of this vertex
     *
     * @param val is marked
     */
    public void setMark(boolean val) {
        isMarked = val;
    }

    /**
     * get parent vertex of this vertex
     *
     * @return parent vertex
     */
    public Vertex getParentVertex() {
        return parentVertex;
    }

    /**
     * set parent vertex of this vertex
     *
     * @param parent parent vertex
     */
    public void setParentVertex(Vertex parent) {
        this.parentVertex = parent;
    }

    /**
     * get min distance from this vertex to source vertex
     *
     * @return min distance
     */
    public double getSDistance() {
        return sDistance;
    }

    /**
     * get name view of this vertex
     *
     * @return name view - text
     */
    public Text getNameView() {
        return nameView;
    }

    /**
     * set min distance from this vertex to source vertex
     *
     * @return min distance
     */
    public void setsDistance(double sDistance) {
        this.sDistance = sDistance;
    }

    /**
     * set highlight for this vertex
     *
     * @param answer is highlight?
     */
    public void setHighLight(boolean answer) {
        if (answer) {
            if (!getStyleClass().contains("highlight")) {
                getStyleClass().add("highlight");
            }
        } else {
            getStyleClass().remove("highlight");
        }
    }

    /**
     * This vertex is selected?
     *
     * @return is selected
     */
    public boolean isSelected() {
        return getStyleClass().contains("selected");
    }

    /**
     * set selected for this vertex
     *
     * @param val is selected?
     */
    public void setSelected(boolean val) {
        if (val) {
            if (!getStyleClass().contains("selected")) {
                getStyleClass().add("selected");
            }
        } else {
            getStyleClass().remove("selected");
        }
    }

    /**
     * set error status for this vertex
     *
     * @param val is error
     */
    public void setError(boolean val) {
        if (val) {
            if (!getStyleClass().contains("error")) {
                getStyleClass().add("error");
            }
        } else {
            getStyleClass().remove("error");
        }
    }

    /**
     * set this vertex as source vertex?
     *
     * @param val is source vertex
     */
    public void setSourceVertex(boolean val) {
        if (val) {
            if (!getStyleClass().contains("source")) {
                getStyleClass().add("source");
            }
        } else {
            getStyleClass().remove("source");
        }
    }

    /**
     * set this vertex as target vertex?
     *
     * @param val is target vertex
     */
    public void setTargetVertex(boolean val) {
        if (val) {
            if (!getStyleClass().contains("target")) {
                getStyleClass().add("target");
            }
        } else {
            getStyleClass().remove("target");
        }
    }

    /**
     * initialize for this vertex
     */
    public void initialize() {
        setMark(false);
        setParentVertex(null);
        setsDistance(Double.POSITIVE_INFINITY);       //  infinite - can not go to there from source
        setHighLight(false);
        setSelected(false);
        setError(false);
        setSourceVertex(false);
        setTargetVertex(false);
    }

    @Override
    protected void finalize() throws Throwable {
        if (graph != null) {
            graph.getChildren().removeAll(nameView, this);
        }
        super.finalize();
    }

    @Override
    public int compareTo(Vertex v) {
        if (sDistance < v.sDistance) {
            return -1;
        } else if (sDistance > v.sDistance) {
            return 1;
        } else {
            return hashCode() - v.hashCode();
        }
    }
}

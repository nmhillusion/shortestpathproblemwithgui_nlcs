package GraphComponent;

import GraphComponent.ListenerPackage.OnDragAndDropVertexListener;
import GraphComponent.ListenerPackage.OnEditEdge;
import GraphComponent.ListenerPackage.OnEditVertex;
import GraphComponent.ListenerPackage.OnCreateEdgeListener;
import GraphComponent.ListenerPackage.OnTraversalVertex;
import AlertPackage.Alert;
import AlertPackage.OnPromptClickListener;
import AlertPackage.Prompt;
import ShortestPathProplemWithGUI.ShortestPathProplemWithGUI;
import ShortestPathProplemWithGUI.MODE_MENU;
import UtilsPackage.Setting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Scale;

/**
 *
 * @author nmhillusion
 */
public class Graph extends Pane
        implements OnCreateEdgeListener,
        OnDragAndDropVertexListener,
        OnEditVertex,
        OnEditEdge {

    private final List<Edge> edgesList;
    private final List<Vertex> verticesList;

    private final static Line tempLine = new Line();
    private final ImageView map = new ImageView();
    private Vertex tempParentSourceVertex = null;
    private static double WIDTH, HEIGHT;
    private Vertex sourceVertex, targetVertex;
    private boolean isDragging = false;
    private final Scale scale = new Scale(1, 1, 0, 0);
    private double originalWidth, originalHeight;

    public Graph() {
        super();
        getStyleClass().add("graph");
        WIDTH = ShortestPathProplemWithGUI.getInstance().getScrollGraphWidth();
        HEIGHT = ShortestPathProplemWithGUI.getInstance().getScrollGraphHeight();

        setPrefSize(WIDTH, HEIGHT);
        setOriginWidth(WIDTH);
        setOriginWidth(HEIGHT);
        
        getChildren().add(0, map);
        map.setSmooth(true);
        
        setScaleShape(true);
        getTransforms().add(scale);

        scale.setOnTransformChanged((event) -> {
            setGraphWidth(getOriginWidth() * scale.getX(), true);
            setGraphHeight(getOriginHeight() * scale.getY(), true);
        });

        edgesList = new ArrayList<>();
        verticesList = new ArrayList<>();

        /**
         * function to create a vertex
         */
        setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (!(event.getPickResult().getIntersectedNode() instanceof Vertex) &&
                        ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.BUILDING) {
                    Vertex v = new Vertex(event.getX(), event.getY(), Graph.this);
                    getChildren().add(v);
                    reqName(v);
                }
            }

            private void reqName(Vertex v) {
                Prompt.getIntance()
                        .setTitle("Name of Vertex")
                        .setHeaderText("Please input vertex's name")
                        .setListener(new OnPromptClickListener() {
                            @Override
                            public void onPositive(String val) {
                                if (!val.matches("([aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ0-9\\_\\-\\s])*")) {
                                    Alert.getIntance()
                                            .setTitle("Error in set name")
                                            .setContent("Name is only contains characters, numbers, space, undercore or dash.")
                                            .show();
                                    reqName(v);
                                    return;
                                }
                                v.setName(val);
                                SAME_TYPE_VERTEX typeSame = containsVertex(v);
                                switch (typeSame) {
                                    case SAME_NAME:
                                        Alert.getIntance()
                                                .setTitle("Error")
                                                .setHeaderText("This name vertex has existed! Please change it...")
                                                .setContent("Please input another name!")
                                                .show();
                                        reqName(v);
                                        break;
                                    case SAME_COORDINATE:
                                        Alert.getIntance()
                                                .setTitle("Error")
                                                .setHeaderText("This position has existed a vertex!")
                                                .setContent("Please change another position!")
                                                .show();
                                        onDeleteVertex(v);
                                        break;
                                    case NOT_SAME:
                                        //  add to list vertex of this graph
                                        verticesList.add(v);
                                        ShortestPathProplemWithGUI.getInstance()
                                                .sendMessageOutput("created " + v);
                                        break;
                                }
                            }

                            @Override
                            public void onNegative() {
                                onDeleteVertex(v);
                            }

                            @Override
                            public void onNullValue() {
                                Alert.getIntance()
                                        .setTitle("Error")
                                        .setHeaderText("Name can not null")
                                        .setContent("Please fill name of vertex!")
                                        .show();
                                reqName(v);
                            }
                        }).show();
            }
        });

        {
            tempLine.setId("temp-edge");
            tempLine.setStrokeDashOffset(10);
            getChildren().add(tempLine);
        }
        /**
         * function to display when mouse move
         */
        setOnMouseDragOver((event) -> {
            if (isDragging) {
                tempLine.setEndX(event.getX());
                tempLine.setEndY(event.getY());
                tempLine.setVisible(isDragging);
                setCursor(Cursor.CLOSED_HAND);
            }
        });

        setOnMouseDragReleased((event) -> {
            onDrop(event);
        });

    }

    public void onKeyPressed(KeyEvent event) {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EDITTING) {
            if (event.getCode() == KeyCode.DELETE) {
                List<Vertex> deleteVerticesList = new ArrayList<>();
                verticesList.forEach((vertex) -> {
                    if (vertex.isSelected()) {
                        deleteVerticesList.add(vertex);
                    }
                });

                deleteVerticesList.forEach((vertex) -> {
                    onDeleteVertex(vertex);
                });

                List<Edge> deleteEdgeList = new ArrayList<>();
                edgesList.forEach((edge) -> {
                    if (edge.isSelected()) {
                        deleteEdgeList.add(edge);
                    }
                });
                deleteEdgeList.forEach((edge) -> {
                    onDeleteEdge(edge);
                });
            }
        }
    }

    public Graph(List<Edge> listE) {
        this();
        listE.forEach((t) -> {
            addEdge(t);
        });
    }
    
    /**
     * refresh skin for graph
     */
    public void refreshSkin(){
        if(Setting.getTypeOfSkin() == Setting.SKIN.DARK){
            tempLine.getStyleClass().add("dark-skin");
        }else{
            tempLine.getStyleClass().remove("dark-skin");
        }
        verticesList.forEach((vertex) -> {
            vertex.refreshSkin();
        });
        edgesList.forEach((edge) -> {
            edge.refreshSkin();
        });
    }
    
    /**
     * get map of this Graph
     * @return image instance of map
     */
    public final ImageView getMap(){
        return map;
    }
    
    /**
     * set map of this graph
     * @param value image to turn into map
     */
    public final void setMap(Image value){
        if(value!=null){
            map.setImage(value);
            setGraphWidth(value.getWidth());
            setGraphHeight(value.getHeight());
            map.setVisible(true);
            
            setOriginWidth(value.getWidth());
            setOriginHeight(value.getHeight());
        }else{
            map.setVisible(false);
        }
    }

    /**
     * set scale x for this graph
     *
     * @param x scale x
     */
    public void setGraphScaleX(double x) {
        scale.setX(x);
    }

    /**
     * set scale y for this graph
     *
     * @param y scale y
     */
    public void setGraphScaleY(double y) {
        scale.setY(y);
    }

    /**
     * get origin width
     *
     * @return origin width
     */
    public double getOriginWidth() {
        return originalWidth;
    }

    /**
     * get origin height
     *
     * @return origin height
     */
    public double getOriginHeight() {
        return originalHeight;
    }

    /**
     * set original width
     *
     * @param width original width
     */
    public final void setOriginWidth(double width) {
        originalWidth = Math.max(originalWidth, width);
    }

    /**
     * set original height
     *
     * @param height original height
     */
    public final void setOriginHeight(double height) {
        originalHeight = Math.max(originalHeight, height);
    }

    /**
     * set width for this graph
     *
     *
     * @param width width will be set
     */
    public void setGraphWidth(double width) {
        setGraphWidth(width, false);
    }

    /**
     * set height for this graph
     *
     * @param height height will be set
     */
    public void setGraphHeight(double height) {
        setGraphHeight(height, false);
    }

    /**
     * set width for this graph
     *
     *
     * @param width width will be set
     * @param force is force
     */
    public void setGraphWidth(double width, boolean force) {
        width += 10;
        if (force || width > WIDTH) {
            this.setPrefWidth(width);
            WIDTH = width;
        }
    }

    /**
     * set height for this graph
     *
     * @param height height will be set
     * @param force is force
     */
    public void setGraphHeight(double height, boolean force) {
        height += 10;
        if (force || height > HEIGHT) {
            this.setPrefHeight(height);
            HEIGHT = height;
        }
    }

    /**
     * get list edge of this graph
     *
     * @return list edge of this graph
     */
    public List<Edge> getListEdge() {
        return edgesList;
    }

    /**
     * get list vertex of this graph
     *
     * @return list vertex of this graph
     */
    public List<Vertex> getListVertex() {
        return verticesList;
    }

    /**
     * set all vertex for this graph
     *
     * @param vertexs list vertices to add
     */
    public void setAllVertex(List<Vertex> vertexs) {
        getChildren().removeAll(verticesList);

        verticesList.clear();
        verticesList.addAll(vertexs);

        getChildren().addAll(verticesList);

        double maxWidth = Collections.max(vertexs, (v1, v2) -> {
            if (v1.getX() - v2.getX() > 0) {
                return 1;
            } else if (v1.getX() - v2.getX() == 0) {
                return 0;
            } else {
                return -1;
            }
        }).getX() + 20,
                maxHeight = Collections.max(vertexs, (v1, v2) -> {
                    if (v1.getY() - v2.getY() > 0) {
                        return 1;
                    } else if (v1.getY() - v2.getY() == 0) {
                        return 0;
                    } else {
                        return -1;
                    }
                }).getY() + 20;

        setGraphWidth(maxWidth);
        setGraphHeight(maxHeight);

        setOriginWidth(maxWidth);
        setOriginHeight(maxHeight);
    }

    public void setAllEdge(List<Edge> edges) {
        getChildren().removeAll(edgesList);

        edgesList.clear();
        edgesList.addAll(edges);

        getChildren().addAll(edgesList);
        setToBackChildren(edgesList);
    }

    /**
     * set toBack() for some children of this graph
     *
     * @param nodes which children to set toBack()
     */
    public void setToBackChildren(List<? extends Shape> nodes) {
        nodes.forEach((node) -> {
            if (getChildren().contains(node)) {
                node.toBack();
            }
        });
        
        map.toBack();
    }

    /**
     * get source vertex
     *
     * @return source vertex
     */
    public Vertex getSourceVertex() {
        return sourceVertex;
    }

    /**
     * get target vertex
     *
     * @return target vertex
     */
    public Vertex getTargetVertex() {
        return targetVertex;
    }

    /**
     * check and type contains vertex of this graph
     *
     * @param vertex vertex to check
     * @return type of same of this vertex
     */
    public SAME_TYPE_VERTEX containsVertex(Vertex vertex) {
        for (Vertex v : verticesList) {
            if (v.getSameType(vertex) != SAME_TYPE_VERTEX.NOT_SAME) {
                return v.getSameType(vertex);
            }
        }
        return SAME_TYPE_VERTEX.NOT_SAME;
    }

    /**
     * get list edges contains a vertex
     *
     * @param vertex a vertex to find edges contains it
     * @return list edges contains checked vertex
     */
    public List<Edge> getListEdgeContainsVertex(Vertex vertex) {
        List<Edge> result = new ArrayList<>();

        edgesList.forEach((edge) -> {
            if (edge.contains(vertex)) {
                result.add(edge);
            }
        });

        return result;
    }

    /**
     * check if an edge is exist in graph
     *
     * @param edge edge to check
     * @return edge in this graph or null if not exist
     */
    public Edge containsEdge(Edge edge) {
        for (Edge e : edgesList) {
            if (e.isSame(edge)) {
                return e;
            }
        }
        return null;
    }

    /**
     * check adjacent between 2 vertex
     *
     * @param v1 first vertex
     * @param v2 second vertex
     * @return is adjacent
     */
    public boolean isAdjacent(Vertex v1, Vertex v2) {
        return containsEdge(new Edge(v1, v2, this)) != null;
    }

    /**
     * add a vertex to this graph
     *
     * @param v vertex to add
     */
    private void addVertex(Vertex v) {
        if (containsVertex(v) == SAME_TYPE_VERTEX.NOT_SAME) {
            verticesList.add(v);
        }
    }

    /**
     * add an edge to this graph
     *
     * @param edge edge to add
     */
    public void addEdge(Edge edge) {
        if (containsEdge(edge) == null) {
            edgesList.add(edge);
            addVertex(edge.getStartV());
            addVertex(edge.getEndV());
        } else {
            Alert.getIntance()
                    .setTitle("Error when create Edge")
                    .setHeaderText("Can not add " + edge + ", because this edge is existed!")
                    .show();
        }
    }

    /**
     * get list adjacent of a vertex
     *
     * @param v vertex to check
     * @return list adjacent of vertex v
     */
    public List<Vertex> getVertexAdjacent(Vertex v) {
        List<Vertex> res = new ArrayList<>();
        edgesList.forEach((t) -> {
            if (t.contains(v)) {
                if (t.getStartV().isSame(v)) {
                    res.add(t.getEndV());
                } else {
                    res.add(t.getStartV());
                }
            }
        });
        return res;
    }

    /**
     * get degree of a vertex
     *
     * @param vertex vertex to compute degree
     * @return degree of vertex v
     */
    public int getDegreeOf(Vertex vertex) {
        return getVertexAdjacent(vertex).size();
    }

    /**
     * traversal via a vertex // Depth First Search
     *
     * @param vertex vertex to traversal
     * @param listener callback when traversal a vertex
     */
    public void traversal(Vertex vertex, OnTraversalVertex listener) {
        if (!vertex.isMarked()) {
            vertex.setMark(true);
            listener.traversal(vertex);

            List<Vertex> listAdj = getVertexAdjacent(vertex);
            listAdj.forEach((v) -> {
                if (!v.isMarked()) {
                    traversal(v, listener);
                }
            });
        }
    }

    /**
     * traversal via a vertex // Breadth First Search
     *
     * @param vertex vertex to traversal
     * @param listener callback when traversal a vertex
     * @see http://www.geeksforgeeks.org/breadth-first-traversal-for-a-graph/
     * @see https://brilliant.org/wiki/depth-first-search-dfs/
     */
    public void traversalBFS(Vertex vertex, OnTraversalVertex listener) {
        LinkedList<Vertex> queueVertex = new LinkedList<>();

        queueVertex.add(vertex);
        while (!queueVertex.isEmpty()) {
            //  Dequeue a vertex from queue and traversal it
            Vertex v = queueVertex.poll();
            if (v != null && !v.isMarked()) {
                v.setMark(true);
                listener.traversal(v);
                queueVertex.addAll(getVertexAdjacent(v).stream().filter((v2c) -> {
                    return !v2c.isMarked();
                }).collect(Collectors.toList()));
            }
        }
    }

    /**
     * reset display graph
     */
    public void resetDisplayGraph() {
        initBasic();
        sourceVertex = targetVertex = null;
    }

    /**
     * initialize default status of vertices and edges
     */
    private void initBasic() {
        verticesList.forEach((v) -> {
            v.initialize();
        });
        edgesList.forEach((e) -> {
            e.initialize();
        });
        
        if (sourceVertex != null) {
            sourceVertex.setSourceVertex(false);
        }
        if (targetVertex != null) {
            targetVertex.setTargetVertex(false);
        }
    }

    /**
     * initial highlight for vertices and edges of this graph
     */
    public void initHighlight() {
        verticesList.forEach((v) -> {
            v.setHighLight(false);
        });
        edgesList.forEach((e) -> {
            e.setHighlight(false);
        });
    }

    /**
     * check this graph is exist negative edge
     *
     * @return is exist negative edge?
     */
    private boolean isExistNegetiveEdge() {
        return edgesList.stream().anyMatch((edge) -> (edge.getWeight() < 0));
    }

    /**
     * run Dijkstra on this graph via a vertex to another vertex
     *
     * @param sourceVertex source vertex when run
     * @param targetVertex target vertex when run
     * @return this graph after run
     */
    private Graph Dijsktra(Vertex sourceVertex, Vertex targetVertex) {
        //  init graph
        initBasic();

        System.out.println("Dijsktra from " + sourceVertex + " to " + targetVertex);
        //  init value for source vertex
        sourceVertex.setParentVertex(null);
        sourceVertex.setsDistance(0);

        TreeSet<Vertex> unmarkedVertex = new TreeSet<>(verticesList.subList(0, verticesList.size()));
        
        //  main algorithm
        Vertex mainVertex;   //  main vertex
        while (!unmarkedVertex.isEmpty()) {

            //  find shortest vertex and
            //  remove it from unmarkedVertex list
            mainVertex = unmarkedVertex.pollFirst();

            //  If not exist nxtVertex, do nothing
            if (mainVertex == null) {
                continue;
            }

            //  mark main vertex is marked to not traversal in future
            mainVertex.setMark(true);

            //  FINISH WHEN FIND TO TARGET VERTEX
            if (targetVertex instanceof Vertex && targetVertex.isSame(mainVertex)) {
                return this;
            }

            //  update remain vertex which is adjacent of mainVertex
            for (Vertex vAdj : getVertexAdjacent(mainVertex)) {
                if (vAdj.isMarked()) {
                    continue;
                }
                /**
                 * condition to update this adjacent vertex is: :: current
                 * sDistance of vAdj is greater or equal than (mainVertex's
                 * sDistance + distance of edge(vAdj, mainVertex))
                 */
                Edge tmpEdge = containsEdge(new Edge(mainVertex, vAdj, this));  //  find edge(nxt, vAdj) in this graph
                if (tmpEdge instanceof Edge
                        && vAdj.getSDistance() > (mainVertex.getSDistance() + tmpEdge.getWeight())) {

                    unmarkedVertex.remove(vAdj);
                    vAdj.setParentVertex(mainVertex);
                    vAdj.setsDistance(mainVertex.getSDistance() + tmpEdge.getWeight());
                    unmarkedVertex.add(vAdj);
                }
            }
        }
        return this;
    }

    /**
     * get shortest path between source vertex and target vertex
     *
     * @param sourceVertex source vertex
     * @param targetVertex target vertex
     * @return list contains vertex from source vertex to target vertex
     */
    public List<Vertex> getShortestPath(Vertex sourceVertex, Vertex targetVertex) {
        if (isExistNegetiveEdge()) {
            Vertex startCycle = existNegativeCycle(sourceVertex);
            if (startCycle instanceof Vertex) {
                Alert.getIntance()
                        .setTitle("Exist negativge cycle")
                        .setHeaderText("Can not find shortest path, because existing negative cycle!")
                        .show();
                ShortestPathProplemWithGUI.getInstance().sendMessageOutput("Exist negative cycle: ");
                StringBuilder stringBuilder = new StringBuilder();
                Vertex temp = startCycle, parentTemp;
                do {
                    parentTemp = !temp.isSame(sourceVertex) ? temp.getParentVertex() : tempParentSourceVertex;
                    stringBuilder.append(temp.getName()).append(" -> ");
                    Edge edge = containsEdge(new Edge(temp, parentTemp, null));
                    temp.setError(true);
                    if (edge instanceof Edge) {
                        edge.setError(true);
                    }

                    temp = parentTemp;
                } while (temp instanceof Vertex && !temp.isSame(startCycle));
                stringBuilder.append(startCycle.getName());

                ShortestPathProplemWithGUI.getInstance().sendMessageOutput(stringBuilder.toString());
                return new ArrayList<>();
            }
        } else {
            Dijsktra(sourceVertex, targetVertex);
        }

        List<Vertex> result = new ArrayList<>();
        Vertex vertex = targetVertex;

        while (vertex instanceof Vertex && !vertex.isSame(sourceVertex)) {
            result.add(vertex);
            vertex = vertex.getParentVertex();

            if (!(vertex instanceof Vertex)) {
                return new ArrayList<>();
            }
        }

        if (!result.isEmpty()) {
            result.add(sourceVertex);   //  have to add source vertex because {while} stop in this vertex
            Collections.reverse(result);
        }

        return result;
    }

    /**
     * run Dijkstra on this graph via a vertex to each another vertex
     *
     * @param sourceVertex source vertex when run
     * @return this graph after run
     */
    public Graph Dijsktra(Vertex sourceVertex) {
        return Dijsktra(sourceVertex, null);
    }

    /**
     * Bellman-Ford algorithm, for situation graph contains negative weight edge
     *
     * @param sourceVertex source vertex
     */
    private void Bellman_Ford(Vertex sourceVertex) {
        // initialize 
        initBasic();
        tempParentSourceVertex = null;
        // init for source vertex
        sourceVertex.setsDistance(0);
        sourceVertex.setParentVertex(null);

        // main iteration
        int numV = verticesList.size();

        for (int i = 1; i < numV; ++i) {
            //  consider each edge
            edgesList.forEach((edge) -> {
                Vertex x = edge.getStartV(),
                        y = edge.getEndV();
                double weight = edge.getWeight();

                // y  go from x will have less distance
                if (x.getSDistance() != Double.POSITIVE_INFINITY) {
                    if (x.getSDistance() + weight < y.getSDistance()
                            && (x.getParentVertex() == null || !x.getParentVertex().isSame(y))) {
                        if (!y.isSame(sourceVertex)) {
                            double temp = y.getSDistance();
                            y.setsDistance(x.getSDistance() + weight);
                            y.setParentVertex(x);
                        } else {
                            tempParentSourceVertex = x;
                        }
                    }
                }
                if (y.getSDistance() != Double.POSITIVE_INFINITY) {
                    // x  go from y will have less distance
                    if (y.getSDistance() + weight < x.getSDistance()
                            && (y.getParentVertex() == null || !y.getParentVertex().isSame(x))) {
                        if (!x.isSame(sourceVertex)) {
                            double temp = x.getSDistance();
                            x.setsDistance(y.getSDistance() + weight);
                            x.setParentVertex(y);
                        } else {
                            tempParentSourceVertex = y;
                        }
                    }
                }
            });
        }
    }

    /**
     * check this graph is exist negative cycle?
     *
     * @param sourceVertex source vertex
     * @return start vertex of Negative Cycle if exist
     */
    public Vertex existNegativeCycle(Vertex sourceVertex) {
        Bellman_Ford(sourceVertex);

        int numV = verticesList.size();

        for (int i = 1; i < numV; ++i) {
            //  consider each edge
            for (Edge edge : edgesList) {
                Vertex x = edge.getStartV(),
                        y = edge.getEndV();
                double weight = edge.getWeight();

                if (x.getSDistance() + weight < y.getSDistance()
                        && (x.getParentVertex() == null || !x.getParentVertex().isSame(y))) {
                    return x;
                } else if (y.getSDistance() + weight < x.getSDistance()
                        && (y.getParentVertex() == null || !y.getParentVertex().isSame(x))) {
                    return y;
                }
            }
        }

        return null;
    }

    /**
     * get interconnected domain lists by DFS (Depth First Search)
     *
     * @return interconnected domain lists
     */
    public List<List<Vertex>> getInterconnectedDomainDFS() {
        final List<List<Vertex>> result = new ArrayList<>();

        resetDisplayGraph();
        verticesList.forEach((vertex) -> {
            final List<Vertex> itemList = new ArrayList<>();
            if (!vertex.isMarked()) {
                traversal(vertex, new OnTraversalVertex() {
                    @Override
                    public void traversal(Vertex vertex) {
                        itemList.add(vertex);
                    }
                });
            }
            if (itemList.size() > 0) {
                result.add(itemList);
            }
        });

        return result;
    }

    /**
     * get interconnected domain lists by DFS (Breadth First Search)
     *
     * @return interconnected domain lists
     */
    public List<List<Vertex>> getInterconnectedDomainBFS() {
        final List<List<Vertex>> result = new ArrayList<>();

        resetDisplayGraph();
        verticesList.forEach((vertex) -> {
            final List<Vertex> itemList = new ArrayList<>();
            if (!vertex.isMarked()) {
                traversalBFS(vertex, new OnTraversalVertex() {
                    @Override
                    public void traversal(Vertex vertex) {
                        itemList.add(vertex);
                    }
                });
            }
            if (itemList.size() > 0) {
                result.add(itemList);
            }
        });

        return result;
    }

    @Override
    public void onCreateEdge(Edge edge) {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.BUILDING && edge instanceof Edge) {
            if (containsEdge(edge) != null) {
                Alert.getIntance()
                        .setTitle("Error")
                        .setHeaderText("Allow only one edge between 2 vertex!")
                        .show();
            } else {
                getChildren().add(edge);
                reqWeightOfEdge(edge);
                edge.toBack();
                
                map.toBack();
            }
        }
    }

    private void reqWeightOfEdge(Edge edge) {
        Prompt.getIntance()
                .setTitle("Weight of Edge")
                .setHeaderText("If you want to create weight automaticaly, please click \"Auto\" button.")
                .setContent("Please input edge's weight")
                .setAutoButton(true)
                .setListener(new OnPromptClickListener() {
                    @Override
                    public void onPositive(String val) {
                        try {
                            double weight = Double.parseDouble(val);
                            edge.setWeight(weight);
                            edgesList.add(edge);
                            ShortestPathProplemWithGUI.getInstance()
                                    .sendMessageOutput("created " + edge);
                        } catch (NumberFormatException ex) {
                            Alert.getIntance()
                                    .setTitle("Error")
                                    .setHeaderText("Weight must be a number.")
                                    .setContent("Please input weight of edge!")
                                    .show();
                            reqWeightOfEdge(edge);
                        }
                    }

                    @Override
                    public void onNegative() {
                        onDeleteEdge(edge);
                    }

                    @Override
                    public void onNullValue() {
                        edge.execWeight();
                        edgesList.add(edge);
                        ShortestPathProplemWithGUI.getInstance()
                                .sendMessageOutput("created " + edge);
                    }
                }).show();
    }

    @Override
    public void onStartDrag(Vertex startVertex) {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.BUILDING) {
            isDragging = true;
            tempLine.setStartX(startVertex.getX());
            tempLine.setStartY(startVertex.getY());
            tempLine.setEndX(startVertex.getX());
            tempLine.setEndY(startVertex.getY());
            setCursor(Cursor.CLOSED_HAND);
        }
    }

    @Override
    public void onDragOver() {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.BUILDING) {
            setCursor(Cursor.OPEN_HAND);
        }
    }

    @Override
    public void onDrop(MouseDragEvent event) {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.BUILDING) {
            isDragging = false;
            tempLine.setVisible(isDragging);
            tempLine.setEndX(tempLine.getStartX());
            tempLine.setEndY(tempLine.getStartY());
            setCursor(Cursor.DEFAULT);
        }
    }

    @Override
    public void onMoveVertex(Vertex vertex) {
        if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EDITTING) {
            getListEdgeContainsVertex(vertex).forEach((edge) -> {
                if (edge instanceof OnEditVertex) {
                    edge.onMoveVertex(vertex);
                }
            });
        }
    }

    @Override
    public void onDeleteVertex(Vertex vertex) {
        try {
            verticesList.remove(vertex);

            getListEdgeContainsVertex(vertex).forEach((edge) -> {
                if (edge instanceof OnEditVertex) {
                    edge.onDeleteVertex(vertex);
                }
            });
            getChildren().removeAll(vertex);

            ShortestPathProplemWithGUI.getInstance()
                    .sendMessageOutput("deleted " + vertex);

            vertex.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onDeleteEdge(Edge edge) {
        try {
            getChildren().remove(edge);
            edgesList.remove(edge);

            ShortestPathProplemWithGUI.getInstance()
                    .sendMessageOutput("deleted " + edge);

            edge.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onSetSourceVertex(Vertex vertex) {
        verticesList.forEach((v) -> {
            v.setSourceVertex(false);
        });
        sourceVertex = vertex;
        ShortestPathProplemWithGUI.getInstance()
                .sendMessageOutput("set source vertex as " + vertex);
    }

    @Override
    public void onSetTargetVertex(Vertex vertex) {
        verticesList.forEach((v) -> {
            v.setTargetVertex(false);
        });
        targetVertex = vertex;
        ShortestPathProplemWithGUI.getInstance()
                .sendMessageOutput("set target vertex as " + vertex);
    }
}

package GraphComponent;

import AlertPackage.Alert;
import AlertPackage.OnPromptClickListener;
import AlertPackage.Prompt;
import GraphComponent.ListenerPackage.OnEditVertex;
import UtilsPackage.GraphUtils;
import ShortestPathProplemWithGUI.MODE_MENU;
import ShortestPathProplemWithGUI.ShortestPathProplemWithGUI;
import UtilsPackage.Setting;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

/**
 *
 * @author nmhillusion
 */
public class Edge extends Line implements OnEditVertex, Comparable<Edge> {

    private Vertex startV, endV;
    private double weight;
    private String name;
    private final Text lblWeight = new Text();
    private Graph graph;
    private final ContextMenu contextMenu = new ContextMenu();

    public Edge(Vertex startV, Vertex endV, String name, Graph graph) {
        super();
        try {
            if(startV == null || endV == null) return;
            setStartX(startV.getX());
            setStartY(startV.getY());
            setEndX(endV.getX());
            setEndY(endV.getY());
            this.startV = startV;
            this.endV = endV;
            this.graph = graph;
            this.name = name;

            /**
             * set context menu
             */
            {
                MenuItem btnEditWeight = new MenuItem("Edit Weight");
                btnEditWeight.setOnAction(new EventHandler<ActionEvent>() {
                    private void reqWeightOfEdge() {
                        Prompt.getIntance()
                                .setTitle("Weight of Edge")
                                .setHeaderText("If you want to create weight automaticaly, please click \"Auto\" button.")
                                .setContent("Please input edge's weight")
                                .setAutoButton(true)
                                .setListener(new OnPromptClickListener() {
                                    @Override
                                    public void onPositive(String val) {
                                        try {
                                            double weight = Double.parseDouble(val);
                                            setWeight(weight);
                                            ShortestPathProplemWithGUI.getInstance()
                                                .sendMessageOutput("edited weight of " + this + " to " + val);
                                        } catch (NumberFormatException ex) {
                                            Alert.getIntance()
                                                    .setTitle("Error")
                                                    .setHeaderText("Weight must be a number.")
                                                    .setContent("Please input weight of edge!")
                                                    .show();
                                            reqWeightOfEdge();
                                        }
                                    }

                                    @Override
                                    public void onNegative() {
                                    }

                                    @Override
                                    public void onNullValue() {
                                        execWeight();
                                    }
                                }).show();
                    }

                    @Override
                    public void handle(ActionEvent event) {
                        reqWeightOfEdge();
                    }
                });

                MenuItem btnDelete = new MenuItem("Delete");
//            btnDelete.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.SHIFT_DOWN));

                btnDelete.setOnAction((event) -> {
                    this.graph.onDeleteEdge(this);
                });

                contextMenu.getItems().addAll(btnEditWeight, btnDelete);
                contextMenu.setAutoHide(true);
            }

            getStyleClass().add("edge");

            if (graph != null) {
                this.lblWeight.getStyleClass().add("edge-weight");
                graph.getChildren().add(this.lblWeight);
                GraphUtils.getInstance()
                        .moveText(this.lblWeight);
                this.lblWeight.setX((startV.getX() + endV.getX()) / 2);
                this.lblWeight.setY((startV.getY() + endV.getY()) / 2);
                refreshSkin();
            }

            setOnMouseClicked((event) -> {
                if (ShortestPathProplemWithGUI.getModeMenu() == MODE_MENU.EDITTING) {
                    if (graph instanceof Graph) {
                        graph.getListEdge().forEach((edge) -> {
                            if(!edge.isSame(this)){
                                edge.setSelected(false);
                            }
                        });
                    }
                    if(!isSelected()){
                        setSelected(true);
                        contextMenu.show(Edge.this, event.getScreenX(), event.getScreenY());
                    }else{
                        setSelected(false);
                    }
                }
            });

            setOnContextMenuRequested((event) -> {
                contextMenu.show(Edge.this, event.getScreenX(), event.getScreenY());
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Edge(Vertex startV, Vertex endV, Graph graph) {
        this(startV, endV, "", graph);
        if (startV != null && endV != null) {
            setName(startV.getName() + " - " + endV.getName());
        }
    }

    /**
     * auto execute weight of this edge = distance between start and end vertex
     */
    public void execWeight() {
        setWeight(Math.sqrt(Math.pow(startV.getX() - endV.getX(), 2) + Math.pow(startV.getY() - endV.getY(), 2)));
    }

    /**
     * get start vertex of this edge
     *
     * @return start vertex
     */
    public Vertex getStartV() {
        return startV;
    }

    /**
     * get end vertex of this edge
     *
     * @return end vertex
     */
    public Vertex getEndV() {
        return endV;
    }

    /**
     * get weight of this edge
     *
     * @return weight of this edge
     */
    public double getWeight() {
        return weight;
    }

    /**
     * set weight of this edge
     *
     * @param weight weight of this edge
     */
    public void setWeight(double weight) {
        this.weight = weight;
        this.lblWeight.setText(String.format("%.2f", this.weight));
    }

    /**
     * get name of this edge
     *
     * @return name edge
     */
    public String getName() {
        return name;
    }

    /**
     * set name for this edge
     *
     * @param name name of edge
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * set start vertex of this edge
     *
     * @param startV start vertex
     */
    public void setStartV(Vertex startV) {
        this.startV = startV;
        this.setStartX(startV.getX());
        this.setStartY(startV.getY());
    }

    /**
     * set end vertex of this edge
     *
     * @param endV end vertex
     */
    public void setEndV(Vertex endV) {
        this.endV = endV;
        this.setEndX(endV.getX());
        this.setEndY(endV.getY());
    }

    @Override
    public String toString() {
        return "edge " + name + ": [" + startV + ", " + endV + "] : " + weight;
    }

    /**
     * check equals between two edge
     *
     * @param another another edge to check
     * @return is same?
     */
    public boolean isSame(Edge another) {
        if (!(another instanceof Edge)) {
            return false;
        }
        return (startV.isSame(another.startV) && endV.isSame(another.endV))
                || (endV.isSame(another.startV) && startV.isSame(another.endV));
    }

    /**
     * check this edge contains a Vertex
     *
     * @param v vertex to check
     * @return is contains?
     */
    public boolean contains(Vertex v) {
        return startV.isSame(v) || endV.isSame(v);
    }

    /**
     * set highlight status for this edge
     *
     * @param answer is highlight?
     */
    public void setHighlight(boolean answer) {
        if (answer) {
            if (!getStyleClass().contains("highlight")) {
                getStyleClass().add("highlight");
            }
        } else {
            getStyleClass().remove("highlight");
        }
    }

    /**
     * This edge is selected?
     *
     * @return is selected
     */
    public boolean isSelected() {
        return getStyleClass().contains("selected");
    }

    /**
     * set selected status for this edge
     *
     * @param val is selected?
     */
    public void setSelected(boolean val) {
        if (val) {
            if (!getStyleClass().contains("selected")) {
                getStyleClass().add("selected");
            }
        } else {
            getStyleClass().remove("selected");
        }
    }

    /**
     * set error status for this edge
     *
     * @param val is error
     */
    public void setError(boolean val) {
        if (val) {
            if (!getStyleClass().contains("error")) {
                getStyleClass().add("error");
            }
        } else {
            getStyleClass().remove("error");
        }
    }
    
    /**
     * refresh skin for this edge: weight
     */
    public void refreshSkin(){
        if(Setting.getTypeOfSkin() == Setting.SKIN.DARK){
            lblWeight.getStyleClass().add("dark-skin");
            getStyleClass().add("dark-skin");
        }else{
            lblWeight.getStyleClass().remove("dark-skin");
            getStyleClass().remove("dark-skin");
        }
    }

    /**
     * initialize for this vertex
     */
    public void initialize() {
        setHighlight(false);
        setSelected(false);
        setError(false);
    }

    @Override
    protected void finalize() throws Throwable {
        graph.getChildren().remove(lblWeight);
        super.finalize();
    }

    @Override
    public void onMoveVertex(Vertex vertex) {
        if (vertex.isSame(startV)) {
            setStartV(vertex);
        } else if (vertex.isSame(endV)) {
            setEndV(vertex);
        }
        this.lblWeight.setX((startV.getX() + endV.getX()) / 2);
        this.lblWeight.setY((startV.getY() + endV.getY()) / 2);
    }

    @Override
    public void onDeleteVertex(Vertex vertex) {
        try {
            graph.getListEdge().remove(this);
            graph.getChildren().removeAll(this, lblWeight);
            
            ShortestPathProplemWithGUI.getInstance()
                    .sendMessageOutput("deleted " + this);
            
            finalize();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onSetSourceVertex(Vertex vertex) {

    }

    @Override
    public void onSetTargetVertex(Vertex vertex) {

    }

    @Override
    public int compareTo(Edge edge) {
        if (weight > edge.weight) {
            return 1;
        } else if (weight < edge.weight) {
            return -1;
        } else {
            return name.compareTo(edge.name);
        }
    }
}

package ShortestPathProplemWithGUI;

import UtilsPackage.GraphUtils;
import AlertPackage.Alert;
import AlertPackage.Confirm;
import AlertPackage.OnConfirmClickListener;
import GraphComponent.Edge;
import GraphComponent.Graph;
import GraphComponent.Vertex;
import UtilsPackage.FileUtils;
import UtilsPackage.Setting;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.controlsfx.control.Notifications;

/**
 *
 * @author nmhillusion
 */
public class ShortestPathProplemWithGUI
        extends Application {

    private static String stylesheetPath;

    {
        stylesheetPath = getClass().getResource("/Resource/styleMain.css").toExternalForm();
    }

    public static String getStyleSheet() {
        return stylesheetPath;
    }

    private static Image icon;

    {
        icon = new Image(getClass().getResource("/Resource/icon.png").toExternalForm());
    }

    public static Image getIcon() {
        return icon;
    }

    /**
     * DECLARED VARIABLE
     */
    private static Graph graph;
    private final static ScrollPane graphScrollPane = new ScrollPane(graph);
    private final static BorderPane mainPane = new BorderPane();
    private static Scene scene;
    private static ShortestPathProplemWithGUI INSTANCE;
    private final static int WIDTH_SCENE = 800, HEIGHT_SCENE = 600;
    public static Paint colorBackground = Paint.valueOf("#222");

    private static MODE_MENU mode = MODE_MENU.BUILDING;
    private final BorderPane mainMenu = new BorderPane();
    private final ListView<String> outputBox = new ListView<>();
    private final ToggleButton btnBuild = new ToggleButton("BUILD");
    private final Spinner<Integer> zoomTool = new Spinner<>(50, 200, 100, 10);

    public static ShortestPathProplemWithGUI getInstance() {
        return INSTANCE;
    }

    @Override
    public void start(Stage primaryStage) {
        INSTANCE = this;

        scene = new Scene(mainPane);
        scene.getStylesheets().add(stylesheetPath);

        mainPane.getStyleClass().add("main-pane");
        {
            graph = new Graph();
            graphScrollPane.setContent(graph);

            graphScrollPane.getStyleClass().add("graph-scroll-pane");
            graphScrollPane.widthProperty().addListener((observable, oldValue, newValue) -> {
                graph.setGraphWidth(newValue.doubleValue());
                graph.setOriginWidth(newValue.doubleValue());
            });
            graphScrollPane.heightProperty().addListener((observable, oldValue, newValue) -> {
                graph.setGraphHeight(newValue.doubleValue());
                graph.setOriginHeight(newValue.doubleValue());
            });

            graphScrollPane.setOnKeyPressed((event) -> {
                graph.onKeyPressed(event);
                if (event.isControlDown()) {
                    if (event.getCode() == KeyCode.EQUALS) {
                        zoomTool.increment();
                    } else if (event.getCode() == KeyCode.MINUS) {
                        zoomTool.decrement();
                    }
                }
            });

            mainPane.setCenter(graphScrollPane);

            Label title = new Label("Shortest Path");
            title.getStyleClass().add("title-header");
            title.setEffect(new Reflection(0, 0.25, 0.5, 0));
            zoomTool.valueProperty().addListener((observable, oldValue, newValue) -> {
                graph.setGraphScaleX((double) newValue / 100);
                graph.setGraphScaleY((double) newValue / 100);
            });
            zoomTool.setPrefSize(80, 30);
            zoomTool.getStyleClass().add("zoom-tool");
            zoomTool.setEditable(true);
            Button zoomIn = new Button(),
                    zoomOut = new Button();
            zoomIn.getStyleClass().add("btn-zoom-tool");
            zoomIn.setStyle("-fx-background-image: url('/Resource/zoom-in.png')");
            zoomOut.getStyleClass().add("btn-zoom-tool");
            zoomOut.setStyle("-fx-background-image: url('/Resource/zoom-out.png')");

            zoomIn.setOnAction((event) -> {
                zoomTool.increment();
            });
            zoomOut.setOnAction((event) -> {
                zoomTool.decrement();
            });

            HBox zoomBox = new HBox(2, zoomOut, zoomTool, zoomIn);
            zoomBox.setAlignment(Pos.CENTER);

            VBox header = new VBox(3, title, zoomBox);
            header.setAlignment(Pos.CENTER_LEFT);

            outputBox.setDisable(true);
            outputBox.setPrefHeight(150);
            outputBox.getStyleClass().add("output-box");
            Tab outputTab = new Tab("Output", outputBox);
            outputTab.setClosable(false);
            TabPane outputTabPane = new TabPane(outputTab);

            mainPane.setTop(header);
            mainPane.setLeft(mainMenu);
            mainPane.setBottom(outputTabPane);
            BorderPane.setAlignment(header, Pos.CENTER);

            createMainMenu();
            btnBuild.fire();
            zoomTool.getEditor().setText("100");

            Platform.runLater(() -> {
                primaryStage.widthProperty().addListener((observable, oldValue, newValue) -> {
                    graphScrollPane.setPrefWidth(newValue.doubleValue() - mainMenu.getWidth());
                });

                primaryStage.heightProperty().addListener((observable, oldValue, newValue) -> {
                    graphScrollPane.setPrefHeight(newValue.doubleValue() - outputTabPane.getHeight() - ((VBox) mainPane.getTop()).getHeight());
                });
            });
        }

        scene.setFill(Paint.valueOf("#222222"));

        primaryStage.getIcons().add(icon);
        primaryStage.setWidth(WIDTH_SCENE + 30);
        primaryStage.setHeight(HEIGHT_SCENE + 30);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Shortest Path Problem");
        primaryStage.show();
    }

    /**
     * Setting map image for this graph
     */
    private void settingMap() {
        if (Setting.getBackgroundUrl() != null && Setting.getBackgroundUrl().length() > 3) {
            try {
                graph.setMap(new Image(new FileInputStream(Setting.getBackgroundUrl())));
            } catch (Exception ex) {
                Alert.getIntance()
                        .setTitle("ERROR")
                        .setIcon(javafx.scene.control.Alert.AlertType.ERROR)
                        .setHeaderText("Error in setting map: " + ex.getLocalizedMessage())
                        .show();
            }
        } else {
            graph.setMap(null);
        }
    }

    /**
     * get scroll graph height
     *
     * @return scroll graph height
     */
    public double getScrollGraphHeight() {
        return graphScrollPane.getHeight();
    }

    /**
     * get scroll graph width
     *
     * @return scroll graph width
     */
    public double getScrollGraphWidth() {
        return graphScrollPane.getWidth();
    }

    /**
     * add message to output
     *
     * @param message message
     */
    public void sendMessageOutput(String message) {
        outputBox.getItems().add(message);
        if (outputBox.getItems().size() > 20) {
            outputBox.getItems().remove(0, outputBox.getItems().size() - 20);
        }
        outputBox.scrollTo(outputBox.getItems().size() - 1);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public static MODE_MENU getModeMenu() {
        return mode;
    }

    private void createMainMenu() {
        ToggleButton btnEdit = new ToggleButton("EDIT"),
                btnRun = new ToggleButton("RUN");
        Button btnSetting = new Button("Setting Graph"),
                btnReset = new Button("Reset Graph"),
                btnSave = new Button("Save Graph"),
                btnImport = new Button("Import Graph");

        btnBuild.setStyle("-fx-background-image: url('/Resource/build_icon.png');");
        btnEdit.setStyle("-fx-background-image: url('/Resource/edit_icon.png');");
        btnRun.setStyle("-fx-background-image: url('/Resource/run_icon.png');");
        btnSetting.setStyle("-fx-background-image: url('/Resource/setting_icon.png');");

        ToggleGroup toggleGroup = new ToggleGroup();
        toggleGroup.getToggles().addAll(btnBuild, btnEdit, btnRun);

        btnBuild.setOnAction((event) -> {
            sendMessageOutput("You are building graph...");
//            createMenuBuild();
        });

        btnEdit.setOnAction((event) -> {
            sendMessageOutput("You are in editing graph...");
            graph.resetDisplayGraph();

            if (btnEdit.isSelected()) {
                mode = MODE_MENU.EDITTING;
            }
        });

        btnRun.setOnAction((event) -> {
            if (!btnRun.isSelected()) {
                return;
            } else {
                mode = MODE_MENU.EXECUTION;
                sendMessageOutput("You are in running graph...");
            }

            if (graph.getListVertex().size() < 1) {
                Alert.getIntance()
                        .setTitle("Error")
                        .setHeaderText("Do not exist any graph to execute.")
                        .show();
                btnRun.setSelected(false);
                return;
            }

            createMenuRun();
        });

        btnReset.setOnAction((event) -> {
            Confirm.getIntance()
                    .setTitle("Confirm to Reset")
                    .setHeaderText("You sure delete this graph?")
                    .setListener(new OnConfirmClickListener() {
                        @Override
                        public void onPositive() {
                            graph = new Graph();
                            graphScrollPane.setContent(graph);
                            createMainMenu();
                            btnBuild.fire();
                            zoomTool.getEditor().setText("100");

                            sendMessageOutput("has reset graph...");
                            Notifications.create()
                                    .title("Reset!")
                                    .text("you had reset graph.")
                                    .owner(graphScrollPane)
                                    .position(Pos.BOTTOM_RIGHT)
                                    .showInformation();
                        }

                        @Override
                        public void onNegative() {

                        }
                    }).show();
        });

        btnImport.setOnAction((event) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Import Graph");
            fileChooser.setInitialDirectory(new File("./graphs"));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Graph File", "*.gf"));
            File importFile = fileChooser.showOpenDialog(scene.getWindow());
            if (importFile instanceof File) {
                graph = FileUtils.getIntance().readFromFile(importFile.getAbsolutePath());
                if (graph instanceof Graph) {
                    graphScrollPane.setContent(graph);
                    createMainMenu();
                    btnBuild.fire();
                    zoomTool.getEditor().setText("100");

                    settingMap();
                    graph.refreshSkin();

                    Notifications.create()
                            .title("Sucess Import!")
                            .text("Imported graph from " + importFile.getAbsolutePath())
                            .position(Pos.BOTTOM_RIGHT)
                            .showInformation();
                    sendMessageOutput("imported graph...");
                }
            }
        });

        btnSave.setOnAction((event) -> {
            if (graph.getListVertex().isEmpty()) {
                Alert.getIntance()
                        .setTitle("Can not save!")
                        .setHeaderText("Error: Not exist a graph to save!")
                        .show();
            } else {
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save Graph");
                fileChooser.setInitialDirectory(new File("./graphs"));
                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Graph File", "*.gf"));
                File saveFile = fileChooser.showSaveDialog(scene.getWindow());
                if (saveFile instanceof File) {
                    FileUtils.getIntance().saveGraph(saveFile.getAbsolutePath(), graph);
                    sendMessageOutput("saved graph!");
                }
            }
        });

        btnSetting.setOnAction((event) -> {
            Setting.getInstance().openSetting(() -> {
                settingMap();
                graph.refreshSkin();
            });
        });

        Button btnMainMenu = new Button("Main Menu");
        btnMainMenu.setId("btn-main-menu");
        btnMainMenu.setOnAction((event) -> {
            GraphUtils.getInstance().getAnimationListener().onStop();
            createMainMenu();
            graph.resetDisplayGraph();
        });

        mainMenu.setTop(btnMainMenu);
        btnMainMenu.getStyleClass().add("present");
        BorderPane.setAlignment(btnMainMenu, Pos.CENTER);

        VBox boxMenu = new VBox(4, btnBuild, btnEdit, btnRun, btnSetting);
        boxMenu.setAlignment(Pos.CENTER);
        mainMenu.getStyleClass().add("main-menu");
        mainMenu.setCenter(boxMenu);

        VBox ioBox = new VBox(4, btnReset, btnImport, btnSave);
        ioBox.getStyleClass().add("io-box");
        mainMenu.setBottom(ioBox);
    }

    private void createMenuRun() {
        Button btnFindInterDom = new Button("Find Inter. Domains"),
                btnFindShortestpath = new Button("Find Shortest Path"),
                btnReset = new Button("Reset");

        VBox body = new VBox(4, btnFindInterDom, btnFindShortestpath, btnReset);
        body.setAlignment(Pos.CENTER);

        ScrollPane boxInterDom = new ScrollPane();
        boxInterDom.setPrefSize(130, 50);
        boxInterDom.getStyleClass().add("box-inter-dom");
        boxInterDom.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        btnFindInterDom.setStyle("-fx-background-image: url('/Resource/run_item_icon.png');");
        btnFindInterDom.setTooltip(new Tooltip("Find Interconnected Domains"));

        btnFindShortestpath.setStyle("-fx-background-image: url('/Resource/run_item_icon.png');");
        btnReset.setStyle("-fx-background-image: url('/Resource/reset_icon.png');");

        btnFindInterDom.setOnAction((event) -> {
            GraphUtils.getInstance()
                    .getAnimationListener().onStop();

            graph.resetDisplayGraph();
            final List<List<Vertex>> interList = new ArrayList<>();

            javafx.scene.control.Alert dialog = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.CONFIRMATION);
            ObservableList<ButtonType> listButtonType = dialog.getButtonTypes();
            listButtonType.clear();

            ButtonType btnDFS = new ButtonType("Depth First Search", ButtonBar.ButtonData.LEFT),
                    btnBFS = new ButtonType("Breadth First Search", ButtonBar.ButtonData.RIGHT);
            listButtonType.addAll(btnDFS, btnBFS, ButtonType.CANCEL);
            dialog.setTitle("Select Traversal Type");
            dialog.setHeaderText("Please select a type to traversal");
            Optional<ButtonType> resultSelect = dialog.showAndWait();
            if (resultSelect.isPresent()) {
                if (resultSelect.get() == btnDFS) {
                    interList.addAll(graph.getInterconnectedDomainDFS());
                } else if (resultSelect.get() == btnBFS) {
                    interList.addAll(graph.getInterconnectedDomainBFS());
                } else {
                    return;
                }
            }

            List<Button> navIter = new ArrayList<>();
            for (int i = 0; i < interList.size(); ++i) {
                Button nav = new Button("iter #" + (i + 1));
                nav.getStyleClass().add("btn-nav-inter-conn");

                nav.setOnAction((e) -> {
                    GraphUtils.getInstance()
                            .getAnimationListener().onStop();

                    //  init
                    graph.initHighlight();

                    StringBuilder resDisplay = new StringBuilder();
                    sendMessageOutput("Interconnected Domain #" + (navIter.indexOf(nav) + 1));
                    interList.get(navIter.indexOf(nav)).forEach((v) -> {
                        v.setHighLight(true);
                        resDisplay.append(v).append(", ");
                        graph.getListEdgeContainsVertex(v).forEach((edge) -> {
                            edge.setHighlight(true);
                        });
                    });
                    sendMessageOutput(resDisplay.toString());

                    GraphUtils.getInstance()
                            .traversalAnimation(interList.get(navIter.indexOf(nav)));
                });
                navIter.add(nav);
            }

            HBox hbox = new HBox(4);
            hbox.setAlignment(Pos.CENTER);

            hbox.getChildren().addAll(navIter);
            sendMessageOutput(navIter.size() < 2 ? "There is 1 interconnected domain."
                    : ("There are " + navIter.size() + " interconnected domains."));

            Notifications.create()
                    .title("Sucess Execute!")
                    .text(navIter.size() < 2 ? "There is 1 interconnected domain."
                            : ("There are " + navIter.size() + " interconnected domains."))
                    .position(Pos.BOTTOM_RIGHT)
                    .owner(graphScrollPane)
                    .showInformation();

            boxInterDom.setContent(hbox);

            if (!body.getChildren().contains(boxInterDom)) {
                body.getChildren().add(1, boxInterDom);
            }
        });

        btnFindShortestpath.setOnAction((event) -> {
            GraphUtils.getInstance()
                    .getAnimationListener().onStop();

            Vertex sourceVertex = graph.getSourceVertex(),
                    targetVertex = graph.getTargetVertex();

            if (!(sourceVertex instanceof Vertex) && (targetVertex instanceof Vertex)) {
                Vertex firstVertex = graph.getListVertex().get(0);
                Confirm.getIntance()
                        .setTitle("Select Source Vertex")
                        .setHeaderText("You have not set source vertex. Do you want to set " + firstVertex + " as source vertex?")
                        .setListener(new OnConfirmClickListener() {
                            @Override
                            public void onPositive() {
                                findShortestPath(firstVertex, targetVertex);
                            }

                            @Override
                            public void onNegative() {

                            }
                        }).show();
            } else if ((sourceVertex instanceof Vertex) && !(targetVertex instanceof Vertex)) {
                Vertex lastVertex = graph.getListVertex().get(graph.getListVertex().size() - 1);
                Confirm.getIntance()
                        .setTitle("Select Target Vertex")
                        .setHeaderText("You have not set target vertex. Do you want to set " + lastVertex + " as target vertex?")
                        .setListener(new OnConfirmClickListener() {
                            @Override
                            public void onPositive() {
                                findShortestPath(sourceVertex, lastVertex);
                            }

                            @Override
                            public void onNegative() {

                            }
                        }).show();
            } else if (!(sourceVertex instanceof Vertex) && !(targetVertex instanceof Vertex)) {
                Vertex firstVertex = graph.getListVertex().get(0),
                        lastVertex = graph.getListVertex().get(graph.getListVertex().size() - 1);
                Confirm.getIntance()
                        .setTitle("Select Target Vertex")
                        .setHeaderText("You have not set both source and target vertex. Do you want to set " + firstVertex + " as source vertex and set " + lastVertex + " as target vertex?")
                        .setListener(new OnConfirmClickListener() {
                            @Override
                            public void onPositive() {
                                findShortestPath(firstVertex, lastVertex);
                            }

                            @Override
                            public void onNegative() {

                            }
                        }).show();
            } else {
                findShortestPath(sourceVertex, targetVertex);
            }
        });

        btnReset.setOnAction((event) -> {
            graph.resetDisplayGraph();
        });

        mainMenu.getTop().getStyleClass().remove("present");    //  set for "Main Menu" button
        mainMenu.setCenter(body);
    }

    private void findShortestPath(Vertex sourceVertex, Vertex targetVertex) {
        List<Vertex> listShortestVertex = graph.getShortestPath(sourceVertex, targetVertex);
        double weightSum = 0.0;

        sendMessageOutput("Shorest path from " + sourceVertex + " to " + targetVertex + " is:");
        StringBuilder path = new StringBuilder();

        for (Vertex v : listShortestVertex) {
            path.append(v.getName()).append(" -> ");
            v.setHighLight(true);
            if (v.getParentVertex() instanceof Vertex) {
                Edge edge = graph.containsEdge(new Edge(v, v.getParentVertex(), null));
                if (edge instanceof Edge) {
                    edge.setHighlight(true);
                    weightSum += edge.getWeight();
                }
            }
        }

        if (!listShortestVertex.isEmpty()) {
            path.delete(path.lastIndexOf(" -> "), path.length());
            sendMessageOutput(path.toString());

            sendMessageOutput("Sum of Weight: " + String.format("%.2f", weightSum));

            GraphUtils.getInstance()
                    .pathMoveAnimation(listShortestVertex, graph);

            Notifications.create()
                    .title("Sucess Execute!")
                    .text("Found the shortest path from " + sourceVertex + " to " + targetVertex + "\n Sum weight: " + String.format("%.2f", weightSum))
                    .position(Pos.BOTTOM_RIGHT)
                    .owner(graphScrollPane)
                    .showInformation();
        } else {
            sendMessageOutput("Not exist the shortest path in this case!");
            Notifications.create()
                    .title("Executed! Warning!")
                    .text("Cannot found the shortest path from " + sourceVertex + " to " + targetVertex)
                    .position(Pos.BOTTOM_RIGHT)
                    .owner(graphScrollPane)
                    .showWarning();
        }
    }
}
